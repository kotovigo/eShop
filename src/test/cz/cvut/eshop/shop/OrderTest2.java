package cz.cvut.eshop.shop;


import cz.cvut.eshop.shop.Customer;
import cz.cvut.eshop.shop.Order;
import cz.cvut.eshop.shop.ShoppingCart;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Ignore;
import org.junit.Test;


import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;

public class OrderTest2 {

    @Test
    public void testCreateOrderWithNoItemsOrderedTest() {
        // setup
        ShoppingCart cart = new ShoppingCart();
        Order order = new Order(cart);
        // act
        order.create();
        int numberOfItems = order.getItems().size();
        // assert
        assertEquals(numberOfItems,0);
    }

    @Test
    public void testOneItemAndDeleted() {
        // setup
        ShoppingCart cart = new ShoppingCart();
        int num1 = 2122;
        float num2 = 5411210;
        int num3 = 4324;
        String str1 = "PC";
        String str2 = "PCs";

        cart.addItem(new StandardItem(num1,str1,num2,str2,num3));
        Order order = new Order(cart, 0);
        order.getItems().remove(0);
        // act
        // assert
        assertEquals(0, order.getItems().size());
    }

    @Test
    public void testOneItemInserted() {
        // setup
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(79987, "Generic laptop", 20000, "laptops", 200));
        Order order = new Order(cart, 0);
        order.create();
        // act
        // assert
        assertEquals(1, order.getItems().size());
    }

    @Test
    public void testMultipleItemsInserted() {
        // setup
        ShoppingCart cart = new ShoppingCart();
        Order order = new Order(cart, 0);
        order.create();
        int numberOfItems = 100;

        //act

        for (int i = 0; i < numberOfItems*2; i = i + 2) {

            int num1 = i;
            int num2 = i + 1;
            float num3 = (float) (num1 + num2 + 0.5);

            String str1 = (char) num1 + "";
            String str2 = (char) num1 + "";
            cart.addItem(new StandardItem(num1,str1,num3,str2,num2));

        }


        assertEquals(numberOfItems, order.getItems().size());

    }


    //TODO: see Ignore
    @Ignore("method order.getTotalAmount() has variable int totalAmount (line 91) where floats are added, variable totalAmount must use type float")
    @Test
    public void testGetTotalAmount() {
        // setup
        ShoppingCart cart = new ShoppingCart();
        Order order = new Order(cart, 0);
        order.create();
        int numberOfItems = 100;
        float price = 0;
        //act

        for (int i = 0; i < numberOfItems*2; i = i + 2) {
            int num1 = i;
            int num2 = i + 1;
            float num3 = (float) (num1 + num2 + 0.31315);

            price += num3;

            String str1 = (char) num1 + "";
            String str2 = (char) num1 + "";
            cart.addItem(new StandardItem(num1,str1,num3,str2,num2));
        }


        assertEquals(price, order.getTotalAmount());

    }


    //TODO: see Ignore
    @Ignore("applyDiscount can't work properly if getTotalAmount() does not work properly")
    @Test
    public void testApplyDiscount() {
        // setup
        ShoppingCart cart = new ShoppingCart();
        Order order = new Order(cart, 0);
        Customer cust = new Customer(20);
        order.setCustomer(cust);
        int discountFromItems = 0;
        float price = 0;
        //act

        for (int i = 0; i < 10; i++) {
            int num1 = i;
            int num2 = i + 1;
            float num3 = (float) (num1 + num2 + 0.31315);

            price += num3;
            discountFromItems += num2;

            String str1 = (char) num1 + "";
            String str2 = (char) num1 + "";
            cart.addItem(new StandardItem(num1,str1,num3,str2,num2));
        }
        order.create();


        float disc = (float) (20 * 0.2);
        float disc2 = (float) (discountFromItems * 0.2);
        price -= disc;
        price -= disc2;

        order.applyDiscount();

        assertEquals(price,order.getTotalAmount());



    }

}
